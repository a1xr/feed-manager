require('dotenv').config()

const CONNECTION_KEY = Symbol.for("A1XR.Feed.Mongo.Connection");
const MongoClient = require('mongodb').MongoClient;
const BASE_URI = process.env.MONGODB_URI;



var singleton = {
    /* 
     * Mongo Utility: Connect to client */

    ClientConnect: async () => {

        return new Promise((resolve, reject) => {

            var globalSymbols = Object.getOwnPropertySymbols(global);
            var hasConnection = (globalSymbols.indexOf(CONNECTION_KEY) > -1);

            if (!hasConnection) {
                global[CONNECTION_KEY] = MongoClient.connect(BASE_URI, { useNewUrlParser: true });
            }

            resolve(global[CONNECTION_KEY]);
        });
    },

    /* 
     * Mongo Utility: Close client */

    ClientClose: async () => {
        if (global[CONNECTION_KEY]) {
            global[CONNECTION_KEY].close();
        }
        return true;
    }
};

Object.freeze(singleton);

module.exports = singleton;