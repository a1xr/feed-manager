import ExchangeFactory from './exchange/exchangeFactory';
import ExchangeCodes from './exchange/exchangeCodes';
import ExchangeHelpers from './exchange/helpers';
import UniverseService from './symbology/universeService';

module.exports = {
  ExchangeServices: {
    GetExchange : ExchangeFactory,
    ExchangeCodes : ExchangeCodes,
    GetExchangeCode : ExchangeHelpers.getExchangeCode,
    getExchangeCodeAndSymbol : ExchangeHelpers.getExchangeCodeAndSymbol
  },
  UniverseService : new UniverseService()
};



