import "@babel/polyfill";
var expect = require('chai').expect;

import { ExchangeServices, UniverseService } from '../index';

describe('Exchange Object', function () {
    describe('GetExchangeCode', function () {
        it('status', function (done) {
            var exchangeCode = ExchangeServices.GetExchangeCode('BI-BTCUSDT');
            expect(exchangeCode).to.equal('BI');
            done();
        });
    });
    describe('GetExchangeCodeAndSymbol', function () {
        it('status', function (done) {
            var exchangeObject = ExchangeServices.getExchangeCodeAndSymbol('BI-BTCUSDT');
            expect(exchangeObject.exchangeCode).to.equal('BI');
            expect(exchangeObject.exchangeSymbol).to.equal('BTCUSDT');
            done();
        });
    });
    describe('GetExchange', function () {
        it('status', function (done) {
            var api = ExchangeServices.GetExchange(ExchangeServices.ExchangeCodes.BINANCE_CODE);
            expect(api.hasOwnProperty('getExchangeUniverse')).to.equal(true);
            done();
        });
    });
});

describe('Symbology', function () {
    describe('UniverseService', function () {
       describe('getSymbolData', function () {
            it('status', function (done) {
                UniverseService.getEntityData('testZZZ').then(function(result){
                    expect(result).to.equal(null);
                });
                done();
            });
        });
        
        describe('_insertEntityData', function () {
            it('status', function (done) {
                UniverseService._insertEntityData({
                    symbol : 'testZZZ',
                    name : 'Test ZZZ Name',
                    exchanges : [{symbol : 'testZZ', active: false}],
                    alias : []
                }).then(function(result){
                    expect(result.inserted).to.equal(true);
                    done();
                });
            });
        });

        describe('getSymbolData', function () {
            it('status', function (done) {
                UniverseService.getEntityData('testZZZ').then(result =>{
                    //console.log(result);
                    expect(result.symbol).to.equal('testZZZ');
                    done();
                });
            });
        });

        describe('getSymbols : ZZZ Query', function () {
            it('status', function (done) {
                UniverseService.getEntities('ZZZ').then(result =>{
                    //console.log(result);
                    expect(result.length).to.equal(1);
                    done();
                });
            });
        });

        describe('getSymbols : All', function () {
            it('status', function (done) {
                UniverseService.getEntities('ZZZ').then(result =>{
                    //console.log(result);
                    expect(result.length >= 1).to.equal(true);
                    done();
                });
            });
        });

        describe('replaceSymbolData', function () {
            it('status', function (done) {
                UniverseService.replaceEntityData({
                    symbol : 'testZZZ',
                    name : 'Test ZZZ Name',
                    exchanges : [{
                        code : 'testYY',
                        active : true
                    }],
                    alias : []
                }).then(result =>{
                    expect(result.updated).to.equal(true);
                    done();
                });
            });
        });

        describe('updateSymbolData : Existing', function () {
            it('status', function (done) {
                UniverseService.updateEntityData({
                    symbol : 'testZZZ',
                    name : 'Test ZZZ Name',
                    exchanges : [{
                        code : 'testZZ',
                        active : false
                    }],
                    alias : ['testYY']
                }).then(result =>{
                    expect(result.updated).to.equal(true);
                    done();
                });
            });
        });

        describe('getUpdatedSymbolData', function () {
            it('status', function (done) {
                UniverseService.getEntityData('testZZZ').then(result =>{
                    //console.log(result);
                    var exchange = result.exchanges.find((item)=> item.code == 'testZZ')
                    expect(exchange.code).to.equal('testZZ');
                    done();
                });
            });
        });

        describe('removeSymbol', function () {
            it('status', function (done) {
                UniverseService.removeEntity('testZZZ').then(result =>{
                    expect(result.deleted).to.equal(true);
                    done();
                });
            });
        });

        describe('updateSymbolData : New', function () {
            it('status', function (done) {
                UniverseService.updateEntityData({
                    symbol : 'testZZY',
                    name : 'Test ZZZ Name',
                    exchanges : [{code:'testZZ', active: false}],
                    alias : ['testYY']
                }).then(result =>{
                    expect(result.updated).to.equal(true);
                    done();
                });
            });
        });

        describe('removeSymbol : Updated', function () {
            it('status', function (done) {
                UniverseService.removeEntity('testZZY').then(result =>{
                    expect(result.deleted).to.equal(true);
                    done();
                });
            });
        });

    });
});

