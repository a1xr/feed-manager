import ExchangeCodes from './exchangeCodes';
import Binance from 'a1xr-binance-exchange-api';

let getBinanceCredentials = ()=>{
    return {
        apiKey : process.env.BINANCE_API_KEY,
        apiSecret : process.env.BINANCE_SECRET
    };
};

module.exports = function(exchangeCode){
    switch(exchangeCode){
        case ExchangeCodes.BINANCE_CODE: 
            return Binance.GetExchange(getBinanceCredentials());
        default:
            throw 'Exchange Does Not Exist';
    }
    return null;
};

