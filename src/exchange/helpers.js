import ExchangeCodes from './exchangeCodes';

const DELIMITTER = '-';

let getExchangeCode = function(symbol){
    symbol = symbol.toLowerCase(); //normalize
    for(var x in ExchangeCodes){
        var lowCode = ExchangeCodes[x].toLowerCase();
        if(symbol.indexOf(lowCode) == 0){
            return ExchangeCodes[x];
        }
    }
    return null;
};

let getExchangeCodeAndSymbol = function(symbol){
    var exchangeCode = getExchangeCode(symbol);
    var index = exchangeCode.length + DELIMITTER.length;
    var exchangeSymbol = symbol.substring(index);

    return {
        exchangeCode : exchangeCode,
        exchangeSymbol : exchangeSymbol
    };
}

module.exports = {
    getExchangeCode : getExchangeCode,
    getExchangeCodeAndSymbol : getExchangeCodeAndSymbol
}


