import { ClientConnect } from '../db/dbMongo';
import DB_NAMES from '../db/dbNames';
import check from 'check-types';

const COLLECTION_NAME = 'universe';

let validateEntityData = function (entityData) {

    if (!isEntityDataValid(entityData)) {
        throw 'Entity Data is invalid {symbol:string, exchanges: array({code:string, active:boolean}), active: boolean, alias : array(string)}';
    }
    return true;
};

let isEntityDataValid = function (entityData) {

    if(!check.array.of.object(entityData.exchanges)){
        return false;
    }

    return check.all(
        check.map(
            entityData,
            {
                symbol: check.string,
                name: check.string,
                exchanges: check.array,
                alias: check.array
            }
        )
    );
}

class UniverseService {

    async getDb() {

        if (!this.db) {
            var connection = await ClientConnect();
            this.db = connection.db(DB_NAMES.EXCHANGE_FEEDS);
        }

        return this.db;
    }

    async getCollection() {
        var db = await this.getDb();
        return db.collection(COLLECTION_NAME);
    }

    async getEntities(partialSymbol = ''){
        var query = partialSymbol ? {'symbol' : {$regex : '.*'+partialSymbol+'.*'}} : {};
        var collection = await this.getCollection();
        
        return new Promise((resolve, reject)=>{
            collection.find(query).toArray((err, result)=>{
                resolve(result);
            });
        });
    }

    async replaceEntityData(entityData) {

        if (validateEntityData(entityData)) {
            var collection = await this.getCollection();

            return new Promise((resolve, reject) => {
                collection.replaceOne({ "symbol": entityData.symbol }, entityData, (err, response) => {
                    resolve({ updated: response && response.result.n == 1 });
                });
            });
        }
    }

    async _insertEntityData(entityData) {

        if (validateEntityData(entityData)) {
            var collection = await this.getCollection();

            return new Promise((resolve, reject) => {
                collection.insertOne(entityData, (err, response) => {
                    resolve({ inserted: response && response.result.n == 1 });
                });
            });
        }
    }

    async removeEntity(symbol) {
        var collection = await this.getCollection();
        return new Promise((resolve, reject) => {
            collection.deleteOne({ symbol: symbol }, (err, response) => {
                resolve({ deleted: response && response.result.n == 1 });
            });
        });
    }

    async getEntityData(symbol) {
        var collection = await this.getCollection();
        return new Promise((resolve, reject) => {

            collection.findOne({ symbol: symbol }, (err, response) => {
                //console.log(response);
                resolve(response);
            });
        });
    }

    async updateEntityData(newEntityData) {

        if (validateEntityData(newEntityData)) {
            var curEntityData = await this.getEntityData(newEntityData.symbol);
            //console.log(currentData);
            return new Promise(async (resolve, reject) => {

                if (curEntityData == null) {
                    var res = await this._insertEntityData(newEntityData);
                    resolve({
                        updated: res.inserted,
                        action: 'INSERT'
                    });

                } else {
                    var newExchanges = newEntityData.exchanges;
                    var curExchanges = curEntityData.exchanges;

                    for(var i = 0; i <  newExchanges.length; i++){
                        var newExchange = newExchanges[i];
                        var exchangeIndex = curExchanges.findIndex((val)=>val.code == newExchange.code);

                        if(exchangeIndex > -1){
                            curExchanges[exchangeIndex].active = newExchange.active;
                        }else{
                            curExchanges.push(newExchange);
                        }
                    }

                    curEntityData.exchanges = curExchanges; //probably not needed given pointer;

                    var res = await this.replaceEntityData(curEntityData);
                    resolve({
                        updated: res.updated,
                        action: 'UPDATED'
                    });
                }
            });
        }
    }

}

module.exports = UniverseService;

