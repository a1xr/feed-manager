import {SYMBOL_TABLE_NAME} from './constants';

class SymbolModel{

    constructor(db, exchange){
        this.db = db;
        this.exchange = exchange;
    }

    isTableCreated(){

    }

    createTableIfNot(){

        let createTable = `create table if not exists `+SYMBOL_TABLE_NAME+`(
            id INT AUTO_INCREMENT PRIMARY KEY,
            symbol varchar(255) not null
          )`;
          database.query(createTable, (err, result) => {
            if (err) throw err;
          });

    }


    storeDeltas(){

    }
}

module.exports = SymbolModel;





