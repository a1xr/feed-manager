"use strict";

var _constants = require("./constants");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var SymbolModel =
/*#__PURE__*/
function () {
  function SymbolModel(db, exchange) {
    _classCallCheck(this, SymbolModel);

    this.db = db;
    this.exchange = exchange;
  }

  _createClass(SymbolModel, [{
    key: "isTableCreated",
    value: function isTableCreated() {}
  }, {
    key: "createTableIfNot",
    value: function createTableIfNot() {
      var createTable = "create table if not exists " + _constants.SYMBOL_TABLE_NAME + "(\n            id INT AUTO_INCREMENT PRIMARY KEY,\n            symbol varchar(255) not null\n          )";
      database.query(createTable, function (err, result) {
        if (err) throw err;
      });
    }
  }, {
    key: "storeDeltas",
    value: function storeDeltas() {}
  }]);

  return SymbolModel;
}();

module.exports = SymbolModel;