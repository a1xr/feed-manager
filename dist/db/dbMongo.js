"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

require('dotenv').config();

var CONNECTION_KEY = Symbol["for"]("A1XR.Feed.Mongo.Connection");

var MongoClient = require('mongodb').MongoClient;

var BASE_URI = process.env.MONGODB_URI;
var singleton = {
  /* 
   * Mongo Utility: Connect to client */
  ClientConnect: function () {
    var _ClientConnect = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              return _context.abrupt("return", new Promise(function (resolve, reject) {
                var globalSymbols = Object.getOwnPropertySymbols(global);
                var hasConnection = globalSymbols.indexOf(CONNECTION_KEY) > -1;

                if (!hasConnection) {
                  global[CONNECTION_KEY] = MongoClient.connect(BASE_URI, {
                    useNewUrlParser: true
                  });
                }

                resolve(global[CONNECTION_KEY]);
              }));

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    function ClientConnect() {
      return _ClientConnect.apply(this, arguments);
    }

    return ClientConnect;
  }(),

  /* 
   * Mongo Utility: Close client */
  ClientClose: function () {
    var _ClientClose = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (global[CONNECTION_KEY]) {
                global[CONNECTION_KEY].close();
              }

              return _context2.abrupt("return", true);

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function ClientClose() {
      return _ClientClose.apply(this, arguments);
    }

    return ClientClose;
  }()
};
Object.freeze(singleton);
module.exports = singleton;