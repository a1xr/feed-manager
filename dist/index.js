"use strict";

var _exchangeFactory = _interopRequireDefault(require("./exchange/exchangeFactory"));

var _exchangeCodes = _interopRequireDefault(require("./exchange/exchangeCodes"));

var _helpers = _interopRequireDefault(require("./exchange/helpers"));

var _universeService = _interopRequireDefault(require("./symbology/universeService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = {
  ExchangeServices: {
    GetExchange: _exchangeFactory["default"],
    ExchangeCodes: _exchangeCodes["default"],
    GetExchangeCode: _helpers["default"].getExchangeCode,
    getExchangeCodeAndSymbol: _helpers["default"].getExchangeCodeAndSymbol
  },
  UniverseService: new _universeService["default"]()
};