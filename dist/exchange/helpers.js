"use strict";

var _exchangeCodes = _interopRequireDefault(require("./exchangeCodes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var DELIMITTER = '-';

var getExchangeCode = function getExchangeCode(symbol) {
  symbol = symbol.toLowerCase(); //normalize

  for (var x in _exchangeCodes["default"]) {
    var lowCode = _exchangeCodes["default"][x].toLowerCase();

    if (symbol.indexOf(lowCode) == 0) {
      return _exchangeCodes["default"][x];
    }
  }

  return null;
};

var getExchangeCodeAndSymbol = function getExchangeCodeAndSymbol(symbol) {
  var exchangeCode = getExchangeCode(symbol);
  var index = exchangeCode.length + DELIMITTER.length;
  var exchangeSymbol = symbol.substring(index);
  return {
    exchangeCode: exchangeCode,
    exchangeSymbol: exchangeSymbol
  };
};

module.exports = {
  getExchangeCode: getExchangeCode,
  getExchangeCodeAndSymbol: getExchangeCodeAndSymbol
};