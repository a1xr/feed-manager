"use strict";

var _exchangeCodes = _interopRequireDefault(require("./exchangeCodes"));

var _a1xrBinanceExchangeApi = _interopRequireDefault(require("a1xr-binance-exchange-api"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var getBinanceCredentials = function getBinanceCredentials() {
  return {
    apiKey: process.env.BINANCE_API_KEY,
    apiSecret: process.env.BINANCE_SECRET
  };
};

module.exports = function (exchangeCode) {
  switch (exchangeCode) {
    case _exchangeCodes["default"].BINANCE_CODE:
      return _a1xrBinanceExchangeApi["default"].GetExchange(getBinanceCredentials());

    default:
      throw 'Exchange Does Not Exist';
  }

  return null;
};