"use strict";

var _dbMongo = require("../db/dbMongo");

var _dbNames = _interopRequireDefault(require("../db/dbNames"));

var _checkTypes = _interopRequireDefault(require("check-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var COLLECTION_NAME = 'symbols';

var validateSymbolData = function validateSymbolData(symbolData) {
  if (!isSymbolDataValid(symbolData)) {
    throw 'Symbol Data is invalid {symbol:string, exchangeCodes: array(string), active: boolean, alias : array(string)}';
  }

  return true;
};

var isSymbolDataValid = function isSymbolDataValid(symbolData) {
  return _checkTypes["default"].all(_checkTypes["default"].map(symbolData, {
    symbol: _checkTypes["default"].string,
    name: _checkTypes["default"].string,
    exchangeCodes: _checkTypes["default"].array,
    alias: _checkTypes["default"].array
  }));
};

var SymbolService =
/*#__PURE__*/
function () {
  function SymbolService() {
    _classCallCheck(this, SymbolService);
  }

  _createClass(SymbolService, [{
    key: "getDb",
    value: function () {
      var _getDb = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var connection;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (this.db) {
                  _context.next = 5;
                  break;
                }

                _context.next = 3;
                return (0, _dbMongo.ClientConnect)();

              case 3:
                connection = _context.sent;
                this.db = connection.db(_dbNames["default"].EXCHANGE_FEEDS);

              case 5:
                return _context.abrupt("return", this.db);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getDb() {
        return _getDb.apply(this, arguments);
      }

      return getDb;
    }()
  }, {
    key: "getCollection",
    value: function () {
      var _getCollection = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var db;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.getDb();

              case 2:
                db = _context2.sent;
                return _context2.abrupt("return", db.collection(COLLECTION_NAME));

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getCollection() {
        return _getCollection.apply(this, arguments);
      }

      return getCollection;
    }()
  }, {
    key: "getSymbols",
    value: function () {
      var _getSymbols = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3() {
        var partialSymbol,
            query,
            collection,
            _args3 = arguments;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                partialSymbol = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : '';
                query = partialSymbol ? {
                  'symbol': {
                    $regex: '.*' + partialSymbol + '.*'
                  }
                } : {};
                _context3.next = 4;
                return this.getCollection();

              case 4:
                collection = _context3.sent;
                return _context3.abrupt("return", new Promise(function (resolve, reject) {
                  collection.find(query).toArray(function (err, result) {
                    resolve(result);
                  });
                }));

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function getSymbols() {
        return _getSymbols.apply(this, arguments);
      }

      return getSymbols;
    }()
  }, {
    key: "replaceSymbolData",
    value: function () {
      var _replaceSymbolData = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(symbolData) {
        var collection;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (!validateSymbolData(symbolData)) {
                  _context4.next = 5;
                  break;
                }

                _context4.next = 3;
                return this.getCollection();

              case 3:
                collection = _context4.sent;
                return _context4.abrupt("return", new Promise(function (resolve, reject) {
                  collection.replaceOne({
                    "symbol": symbolData.symbol
                  }, symbolData, function (err, response) {
                    resolve({
                      updated: response && response.result.n == 1
                    });
                  });
                }));

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function replaceSymbolData(_x) {
        return _replaceSymbolData.apply(this, arguments);
      }

      return replaceSymbolData;
    }()
  }, {
    key: "_insertSymbolData",
    value: function () {
      var _insertSymbolData2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5(symbolData) {
        var collection;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (!validateSymbolData(symbolData)) {
                  _context5.next = 5;
                  break;
                }

                _context5.next = 3;
                return this.getCollection();

              case 3:
                collection = _context5.sent;
                return _context5.abrupt("return", new Promise(function (resolve, reject) {
                  collection.insertOne(symbolData, function (err, response) {
                    resolve({
                      inserted: response && response.result.n == 1
                    });
                  });
                }));

              case 5:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function _insertSymbolData(_x2) {
        return _insertSymbolData2.apply(this, arguments);
      }

      return _insertSymbolData;
    }()
  }, {
    key: "removeSymbol",
    value: function () {
      var _removeSymbol = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee6(symbol) {
        var collection;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return this.getCollection();

              case 2:
                collection = _context6.sent;
                return _context6.abrupt("return", new Promise(function (resolve, reject) {
                  collection.deleteOne({
                    symbol: symbol
                  }, function (err, response) {
                    resolve({
                      deleted: response && response.result.n == 1
                    });
                  });
                }));

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function removeSymbol(_x3) {
        return _removeSymbol.apply(this, arguments);
      }

      return removeSymbol;
    }()
  }, {
    key: "getSymbolData",
    value: function () {
      var _getSymbolData = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee7(symbol) {
        var collection;
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return this.getCollection();

              case 2:
                collection = _context7.sent;
                return _context7.abrupt("return", new Promise(function (resolve, reject) {
                  collection.findOne({
                    symbol: symbol
                  }, function (err, response) {
                    //console.log(response);
                    resolve(response);
                  });
                }));

              case 4:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function getSymbolData(_x4) {
        return _getSymbolData.apply(this, arguments);
      }

      return getSymbolData;
    }()
  }, {
    key: "updateSymbolData",
    value: function () {
      var _updateSymbolData = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee9(newSymbolData) {
        var _this = this;

        var curSymbolData;
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                if (!validateSymbolData(newSymbolData)) {
                  _context9.next = 5;
                  break;
                }

                _context9.next = 3;
                return this.getSymbolData(newSymbolData.symbol);

              case 3:
                curSymbolData = _context9.sent;
                return _context9.abrupt("return", new Promise(
                /*#__PURE__*/
                function () {
                  var _ref = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee8(resolve, reject) {
                    var res, curExchangeCodes;
                    return regeneratorRuntime.wrap(function _callee8$(_context8) {
                      while (1) {
                        switch (_context8.prev = _context8.next) {
                          case 0:
                            if (!(curSymbolData == null)) {
                              _context8.next = 7;
                              break;
                            }

                            _context8.next = 3;
                            return _this._insertSymbolData(newSymbolData);

                          case 3:
                            res = _context8.sent;
                            resolve({
                              updated: res.inserted,
                              action: 'INSERT'
                            });
                            _context8.next = 14;
                            break;

                          case 7:
                            curExchangeCodes = curSymbolData.exchangeCodes;

                            if (newSymbolData.active === false) {
                              curSymbolData.active = newSymbolData.active;
                            }

                            curSymbolData.exchangeCodes = curExchangeCodes.concat(newSymbolData.exchangeCodes.filter(function (i) {
                              return curExchangeCodes.indexOf(i) == -1;
                            }));
                            _context8.next = 12;
                            return _this.replaceSymbolData(curSymbolData);

                          case 12:
                            res = _context8.sent;
                            resolve({
                              updated: res.updated,
                              action: 'UPDATED'
                            });

                          case 14:
                          case "end":
                            return _context8.stop();
                        }
                      }
                    }, _callee8);
                  }));

                  return function (_x6, _x7) {
                    return _ref.apply(this, arguments);
                  };
                }()));

              case 5:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      function updateSymbolData(_x5) {
        return _updateSymbolData.apply(this, arguments);
      }

      return updateSymbolData;
    }()
  }]);

  return SymbolService;
}();

module.exports = SymbolService;