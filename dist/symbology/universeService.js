"use strict";

var _dbMongo = require("../db/dbMongo");

var _dbNames = _interopRequireDefault(require("../db/dbNames"));

var _checkTypes = _interopRequireDefault(require("check-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var COLLECTION_NAME = 'universe';

var validateEntityData = function validateEntityData(entityData) {
  if (!isEntityDataValid(entityData)) {
    throw 'Entity Data is invalid {symbol:string, exchanges: array({code:string, active:boolean}), active: boolean, alias : array(string)}';
  }

  return true;
};

var isEntityDataValid = function isEntityDataValid(entityData) {
  if (!_checkTypes["default"].array.of.object(entityData.exchanges)) {
    return false;
  }

  return _checkTypes["default"].all(_checkTypes["default"].map(entityData, {
    symbol: _checkTypes["default"].string,
    name: _checkTypes["default"].string,
    exchanges: _checkTypes["default"].array,
    alias: _checkTypes["default"].array
  }));
};

var UniverseService =
/*#__PURE__*/
function () {
  function UniverseService() {
    _classCallCheck(this, UniverseService);
  }

  _createClass(UniverseService, [{
    key: "getDb",
    value: function () {
      var _getDb = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var connection;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (this.db) {
                  _context.next = 5;
                  break;
                }

                _context.next = 3;
                return (0, _dbMongo.ClientConnect)();

              case 3:
                connection = _context.sent;
                this.db = connection.db(_dbNames["default"].EXCHANGE_FEEDS);

              case 5:
                return _context.abrupt("return", this.db);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getDb() {
        return _getDb.apply(this, arguments);
      }

      return getDb;
    }()
  }, {
    key: "getCollection",
    value: function () {
      var _getCollection = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var db;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.getDb();

              case 2:
                db = _context2.sent;
                return _context2.abrupt("return", db.collection(COLLECTION_NAME));

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getCollection() {
        return _getCollection.apply(this, arguments);
      }

      return getCollection;
    }()
  }, {
    key: "getEntities",
    value: function () {
      var _getEntities = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3() {
        var partialSymbol,
            query,
            collection,
            _args3 = arguments;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                partialSymbol = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : '';
                query = partialSymbol ? {
                  'symbol': {
                    $regex: '.*' + partialSymbol + '.*'
                  }
                } : {};
                _context3.next = 4;
                return this.getCollection();

              case 4:
                collection = _context3.sent;
                return _context3.abrupt("return", new Promise(function (resolve, reject) {
                  collection.find(query).toArray(function (err, result) {
                    resolve(result);
                  });
                }));

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function getEntities() {
        return _getEntities.apply(this, arguments);
      }

      return getEntities;
    }()
  }, {
    key: "replaceEntityData",
    value: function () {
      var _replaceEntityData = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(entityData) {
        var collection;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (!validateEntityData(entityData)) {
                  _context4.next = 5;
                  break;
                }

                _context4.next = 3;
                return this.getCollection();

              case 3:
                collection = _context4.sent;
                return _context4.abrupt("return", new Promise(function (resolve, reject) {
                  collection.replaceOne({
                    "symbol": entityData.symbol
                  }, entityData, function (err, response) {
                    resolve({
                      updated: response && response.result.n == 1
                    });
                  });
                }));

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function replaceEntityData(_x) {
        return _replaceEntityData.apply(this, arguments);
      }

      return replaceEntityData;
    }()
  }, {
    key: "_insertEntityData",
    value: function () {
      var _insertEntityData2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5(entityData) {
        var collection;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (!validateEntityData(entityData)) {
                  _context5.next = 5;
                  break;
                }

                _context5.next = 3;
                return this.getCollection();

              case 3:
                collection = _context5.sent;
                return _context5.abrupt("return", new Promise(function (resolve, reject) {
                  collection.insertOne(entityData, function (err, response) {
                    resolve({
                      inserted: response && response.result.n == 1
                    });
                  });
                }));

              case 5:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function _insertEntityData(_x2) {
        return _insertEntityData2.apply(this, arguments);
      }

      return _insertEntityData;
    }()
  }, {
    key: "removeEntity",
    value: function () {
      var _removeEntity = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee6(symbol) {
        var collection;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return this.getCollection();

              case 2:
                collection = _context6.sent;
                return _context6.abrupt("return", new Promise(function (resolve, reject) {
                  collection.deleteOne({
                    symbol: symbol
                  }, function (err, response) {
                    resolve({
                      deleted: response && response.result.n == 1
                    });
                  });
                }));

              case 4:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function removeEntity(_x3) {
        return _removeEntity.apply(this, arguments);
      }

      return removeEntity;
    }()
  }, {
    key: "getEntityData",
    value: function () {
      var _getEntityData = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee7(symbol) {
        var collection;
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return this.getCollection();

              case 2:
                collection = _context7.sent;
                return _context7.abrupt("return", new Promise(function (resolve, reject) {
                  collection.findOne({
                    symbol: symbol
                  }, function (err, response) {
                    //console.log(response);
                    resolve(response);
                  });
                }));

              case 4:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function getEntityData(_x4) {
        return _getEntityData.apply(this, arguments);
      }

      return getEntityData;
    }()
  }, {
    key: "updateEntityData",
    value: function () {
      var _updateEntityData = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee9(newEntityData) {
        var _this = this;

        var curEntityData;
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                if (!validateEntityData(newEntityData)) {
                  _context9.next = 5;
                  break;
                }

                _context9.next = 3;
                return this.getEntityData(newEntityData.symbol);

              case 3:
                curEntityData = _context9.sent;
                return _context9.abrupt("return", new Promise(
                /*#__PURE__*/
                function () {
                  var _ref = _asyncToGenerator(
                  /*#__PURE__*/
                  regeneratorRuntime.mark(function _callee8(resolve, reject) {
                    var res, newExchanges, curExchanges, i, newExchange, exchangeIndex;
                    return regeneratorRuntime.wrap(function _callee8$(_context8) {
                      while (1) {
                        switch (_context8.prev = _context8.next) {
                          case 0:
                            if (!(curEntityData == null)) {
                              _context8.next = 7;
                              break;
                            }

                            _context8.next = 3;
                            return _this._insertEntityData(newEntityData);

                          case 3:
                            res = _context8.sent;
                            resolve({
                              updated: res.inserted,
                              action: 'INSERT'
                            });
                            _context8.next = 15;
                            break;

                          case 7:
                            newExchanges = newEntityData.exchanges;
                            curExchanges = curEntityData.exchanges;

                            for (i = 0; i < newExchanges.length; i++) {
                              newExchange = newExchanges[i];
                              exchangeIndex = curExchanges.findIndex(function (val) {
                                return val.code == newExchange.code;
                              });

                              if (exchangeIndex > -1) {
                                curExchanges[exchangeIndex].active = newExchange.active;
                              } else {
                                curExchanges.push(newExchange);
                              }
                            }

                            curEntityData.exchanges = curExchanges; //probably not needed given pointer;

                            _context8.next = 13;
                            return _this.replaceEntityData(curEntityData);

                          case 13:
                            res = _context8.sent;
                            resolve({
                              updated: res.updated,
                              action: 'UPDATED'
                            });

                          case 15:
                          case "end":
                            return _context8.stop();
                        }
                      }
                    }, _callee8);
                  }));

                  return function (_x6, _x7) {
                    return _ref.apply(this, arguments);
                  };
                }()));

              case 5:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      function updateEntityData(_x5) {
        return _updateEntityData.apply(this, arguments);
      }

      return updateEntityData;
    }()
  }]);

  return UniverseService;
}();

module.exports = UniverseService;